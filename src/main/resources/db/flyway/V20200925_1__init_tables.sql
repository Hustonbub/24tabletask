Create table "users"
(
    users_id            serial primary key,
    authorization_id    bigint,
    name               varchar(128),
    surname            varchar(128),
    secondname         varchar(128),
    status             varchar(128),
    companyUnitId      bigint,
    password           varchar(128),
    last_login_timestamp bigint,
    iin                varchar(32),
    is_active           boolean,
    is_activated        boolean,
    created_timestamp   bigint,
    created_by          bigint,
    updated_timestamp   bigint,
    updated_by          bigint
);
CREATE TABLE "auth"
(
    auth_id serial primary key,
    username  varchar(255),
    email  varchar(255),
    password varchar(128),
    role  varchar(255),
    forgot_password_key  varchar(128),
    forgot_password_key_timestamp bigint,
    company_unit_id bigint
);
CREATE TABLE if not exists "actjournal"
(
    activity_journalId serial primary key,
    activity_type      varchar(128),
    object_type        varchar(255),
    object_id          bigint,
    created_timestamp  bigint,
    created_by         bigint,
    message_level      varchar(128),
    message           varchar(255)
);
CREATE TABLE if not exists "case"
(
    case_id                serial primary key,
    case_number            varchar(128),
    volume_number          varchar(128),
    case_ru                varchar(128),
    case_Kz                varchar(128),
    case_en                varchar(128),
    start_date             bigint,
    finish_date            bigint,
    page_count             bigint,
    is_ecp_signature        boolean,
    ecp_signature          varchar(255),
    is_sentNaf             boolean,
    is_deleted             boolean,
    is_access_restricted    boolean,
    hash                  varchar(128),
    version               integer,
    id_version             varchar(128),
    is_version_active       boolean,
    note                  varchar(255),
    location_id            bigint,
    case_index_id           bigint,
    record_id              bigint,
    destruction_act_id      bigint,
    company_unit_id         bigint,
    case_address_blockchain varchar(128),
    add_date_blockchain     bigint,
    created_timestamp      bigint,
    created_by             bigint,
    updated_timestamp      bigint,
    updated_by             bigint
);

CREATE TABLE if not exists "file"
(
    file_id           serial primary key,
    file_name         varchar(128),
    file_type         varchar(128),
    size             bigint,
    page_count        integer,
    hash             varchar(128),
    is_deleted        boolean,
    file_binary_id     bigint,
    created_timestamp bigint,
    created_by        bigint,
    updated_timestamp bigint,
    updated_by        bigint
);

CREATE TABLE if not exists "fond"
(
    fond_id           serial primary key,
    fond_number       varchar(128),
    created_timestamp bigint,
    created_by        bigint,
    updated_timestamp bigint,
    updated_by        bigint
);


CREATE TABLE if not exists "tempfiles"
(
    tempfiles_id    serial primary key,
    file_binary     varchar(255),
    file_binary_byte bytea
);
