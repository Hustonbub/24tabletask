
CREATE TABLE "filerouting"
(
    file_routing_id serial primary key,
    file_id        bigint,
    table_name     varchar(128),
    table_id       bigint,
    file_type      varchar(128)
);
CREATE TABLE "caseindex"
(
    case_index_id      serial primary key,
    case_index        varchar(128),
    title_ru          varchar(128),
    title_kz          varchar(128),
    title_en          varchar(128),
    storage_type      integer,
    storage_year      integer,
    note             varchar(128),
    company_unit_id    bigint,
    nomenclature_id   bigint,
    created_timestamp bigint,
    created_by        bigint,
    updated_timestamp bigint,
    updated_by        bigint
);

CREATE TABLE"destructionact"
(
    destruction_act_id serial primary key,
    number_act        varchar(128),
    base             varchar(256),
    company_unit_id    bigint,
    created_timestamp bigint,
    created_by        bigint,
    updated_timestamp bigint,
    updated_by        bigint
);

CREATE TABLE  "searchkeyrouting"
(
    search_key_routing_id   serial primary key,
    search_key_id          bigint,
    table_name            varchar(128),
    table_id              bigint,
    search_key_routing_type varchar(128)
);
CREATE TABLE "share"
(
    share_id        serial primary key,
    request_id      bigint,
    note           varchar(255),
    sender_id       bigint,
    receiver_id     bigint,
    share_timestamp bigint
);
CREATE TABLE "catalog"
(
    catalog_id        serial primary key,
    name_ru           varchar(128),
    name_kz           varchar(128),
    name_en           varchar(128),
    parent_id         bigint,
    company_unit_id    bigint,
    created_timestamp bigint,
    created_by        bigint,
    updated_timestamp bigint,
    updated_by        bigint

);


CREATE TABLE  "record"
(
    record_id         serial primary key,
    record_number     varchar(128),
    record_type       varchar(128),
    company_unit_id    bigint,
    created_timestamp bigint,
    created_by        bigint,
    updated_timestamp bigint,
    updated_by        bigint

);


CREATE TABLE "reqstatushist"
(
    request_status_history_id serial primary key,
    request_id              bigint,
    status                 varchar(128),
    created_timestamp       bigint,
    created_by              bigint,
    updated_timestamp       bigint,
    updated_by              bigint

);


CREATE TABLE "catalogcase"
(
    catalog_case_id    serial primary key,
    case_id           bigint,
    catalog_id        bigint,
    company_unit_id    bigint,
    created_timestamp bigint,
    created_by        bigint,
    updated_timestamp bigint,
    updated_by        bigint

);

CREATE TABLE  "request"
(
    request_id        serial primary key,
    request_user_id    bigint,
    response_user_d   bigint,
    case_id           bigint,
    case_index_id      bigint,
    created_type      varchar(64),
    comment          varchar(255),
    status           varchar(64),
    created_timestamp bigint,
    share_start       bigint,
    share_finish      bigint,
    favorite         boolean,
    updated_timestamp bigint,
    updated_by        bigint,
    decline_note      varchar(255),
    company_unit_id    bigint,
    from_request_id    bigint

);

CREATE TABLE  "searchkey"
(
    search_key_id      serial primary key,
    search_key_name    varchar(128),
    company_unit_id    bigint,
    created_timestamp bigint,
    created_by        bigint,
    updated_timestamp bigint,
    updated_by        bigint

);

CREATE TABLE  "company"
(
    company_id        serial primary key,
    name_ru           varchar(128),
    name_kz           varchar(128),
    name_en           varchar(128),
    bin              varchar(32),
    parent_id         bigint,
    fond_id           bigint,
    created_timestamp bigint,
    created_by        bigint,
    updated_timestamp bigint,
    updated_by        bigint
);


CREATE TABLE  "companyunit"
(
    company_unit_id    serial primary key,
    name_ru           varchar(128),
    name_kz           varchar(128),
    name_en           varchar(128),
    parent_id         bigint,
    year             integer,
    company_id        integer,
    code_index        varchar(16),
    created_timestamp bigint,
    created_by        bigint,
    updated_timestamp bigint,
    updated_by        bigint
);

CREATE TABLE "notification"
(
    notification_id   serial primary key,
    object_type       varchar(128),
    object_id         bigint,
    company_unit_id    bigint,
    user_id           bigint,
    created_timestamp bigint,
    viewed_timestamp  bigint,
    is_viewed         boolean,
    title            varchar(255),
    text             varchar(255),
    company_id        bigint
);

CREATE TABLE "nomenclature"
(
    nomenclature_id        serial primary key,
    nomenclature_number    varchar(128),
    year                  integer,
    nomenclature_summary_id bigint,
    company_unit_id         bigint,
    created_timestamp      bigint,
    created_by             bigint,
    updated_timestamp      bigint,
    updated_by             bigint
);


CREATE TABLE "nomenclaturesum"
(
    nomenclature_summary_id     serial primary key,
    nomenclature_summary_number varchar(128),
    year                      integer,
    company_unit_id             bigint,
    created_timestamp          bigint,
    created_by                 bigint,
    updated_timestamp          bigint,
    updated_by                 bigint
);
CREATE TABLE "location"
(
    location_id       serial primary key,
    row              varchar(64),
    line             varchar(64),
    columnn          varchar(64),
    box              varchar(64),
    company_unit_id    bigint,
    created_timestamp bigint,
    created_by        bigint,
    updated_timestamp bigint,
    updated_by        bigint
);