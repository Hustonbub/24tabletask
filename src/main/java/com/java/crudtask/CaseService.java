package com.java.crudtask;

import com.java.crudtask.Case;
import com.java.crudtask.CaseRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CaseService {
    public final CaseRepository caseRepository;

    public CaseService(CaseRepository caseRepository) {
        this.caseRepository = caseRepository;
    }

    public List<Case> getAll(){
        return (List<Case>) caseRepository.findAll();
    }

    public Case getById(Long caseId) {
        return caseRepository.findById(caseId).orElse(null);
    }

    public Case create(Case caze) {
        return caseRepository.save(caze);
    }

    public Case update(Case caze) {
        return caseRepository.save(caze);
    }

    public void delete(Long caseId) {
        caseRepository.deleteById(caseId);
    }
}
