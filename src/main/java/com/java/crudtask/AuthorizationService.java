package com.java.crudtask;

import com.java.crudtask.Authorization;
import com.java.crudtask.AuthorizationRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorizationService {
    public final AuthorizationRepository authorizationRepository;

    public AuthorizationService(AuthorizationRepository authorizationRepository) {
        this.authorizationRepository = authorizationRepository;
    }

    public List<Authorization> getAll(){
        return (List<Authorization>) authorizationRepository.findAll();
    }

    public Authorization getById(Long authorizationId) {
        return authorizationRepository.findById(authorizationId).orElse(null);
    }

    public Authorization create(Authorization authorization) {
        return authorizationRepository.save(authorization);
    }

    public Authorization update(Authorization authorization) {
        return authorizationRepository.save(authorization);
    }

    public void delete(Long authorizationId) {
        authorizationRepository.deleteById(authorizationId);
    }
}









