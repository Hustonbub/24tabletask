package com.java.crudtask;

import com.java.crudtask.CaseIndex;
import com.java.crudtask.CaseIndexRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CaseIndexService {
    public final CaseIndexRepository caseIndexRepository;

    public CaseIndexService(CaseIndexRepository caseIndexRepository) {
        this.caseIndexRepository = caseIndexRepository;
    }

    public List<CaseIndex> getAll(){
        return (List<CaseIndex>) caseIndexRepository.findAll();
    }

    public CaseIndex getById(Long caseIndexId) {
        return caseIndexRepository.findById(caseIndexId).orElse(null);
    }

    public CaseIndex create(CaseIndex caseIndex) {
        return caseIndexRepository.save(caseIndex);
    }

    public CaseIndex update(CaseIndex caseIndex) {
        return caseIndexRepository.save(caseIndex);
    }

    public void delete(Long caseIndexId) {
        caseIndexRepository.deleteById(caseIndexId);
    }
}
