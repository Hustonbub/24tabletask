package com.java.crudtask;

import com.java.crudtask.CatalogCase;
import org.springframework.data.repository.CrudRepository;

public interface CatalogCaseRepository extends CrudRepository<CatalogCase,Long> {
}
