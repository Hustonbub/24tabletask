package com.java.crudtask;

import com.java.crudtask.Case;
import org.springframework.data.repository.CrudRepository;

public interface CaseRepository extends CrudRepository<Case,Long> {
}
