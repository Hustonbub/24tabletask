package com.java.crudtask;

import com.java.crudtask.Request;
import com.java.crudtask.RequestRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RequestService {
    public final RequestRepository requestRepository;

    public RequestService(RequestRepository requestRepository) {
        this.requestRepository = requestRepository;
    }

    public List<Request> getAll(){
        return (List<Request>) requestRepository.findAll();
    }

    public Request getById(Long requestId) {
        return requestRepository.findById(requestId).orElse(null);
    }

    public Request create(Request request) {
        return requestRepository.save(request);
    }

    public Request update(Request request) {
        return requestRepository.save(request);
    }

    public void delete(Long requestId) {
        requestRepository.deleteById(requestId);
    }
}
