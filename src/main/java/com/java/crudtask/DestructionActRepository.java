package com.java.crudtask;

import com.java.crudtask.DestructionAct;
import org.springframework.data.repository.CrudRepository;

public interface DestructionActRepository extends CrudRepository<DestructionAct,Long> {
}