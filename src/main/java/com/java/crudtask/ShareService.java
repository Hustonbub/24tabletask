package com.java.crudtask;

import com.java.crudtask.Share;
import com.java.crudtask.ShareRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShareService {
    public final ShareRepository shareRepository;

    public ShareService(ShareRepository shareRepository) {
        this.shareRepository = shareRepository;
    }

    public List<Share> getAll(){
        return (List<Share>) shareRepository.findAll();
    }

    public Share getById(Long shareId) {
        return shareRepository.findById(shareId).orElse(null);
    }

    public Share create(Share share) {
        return shareRepository.save(share);
    }

    public Share update(Share share) {
        return shareRepository.save(share);
    }

    public void delete(Long shareId) {
        shareRepository.deleteById(shareId);
    }
}
