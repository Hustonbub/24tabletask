package com.java.crudtask;

import com.java.crudtask.CompanyUnit;
import org.springframework.data.repository.CrudRepository;

public interface CompanyUnitRepository extends CrudRepository<CompanyUnit,Long> {
}
