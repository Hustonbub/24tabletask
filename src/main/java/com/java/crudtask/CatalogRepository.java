package com.java.crudtask;

import com.java.crudtask.Catalog;
import org.springframework.data.repository.CrudRepository;

public interface CatalogRepository extends CrudRepository<Catalog,Long> {
}
