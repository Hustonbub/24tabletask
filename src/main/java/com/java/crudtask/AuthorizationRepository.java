package com.java.crudtask;

import com.java.crudtask.Authorization;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AuthorizationRepository extends CrudRepository<Authorization,Long> {
}
