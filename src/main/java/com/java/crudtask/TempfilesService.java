package com.java.crudtask;

import com.java.crudtask.Tempfiles;
import com.java.crudtask.TempfilesRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TempfilesService {
    public final TempfilesRepository tempfilesRepository;

    public TempfilesService(TempfilesRepository tempfilesRepository) {
        this.tempfilesRepository = tempfilesRepository;
    }

    public List<Tempfiles> getAll(){
        return (List<Tempfiles>) tempfilesRepository.findAll();
    }

    public Tempfiles getById(Long tempfilesId) {
        return tempfilesRepository.findById(tempfilesId).orElse(null);
    }

    public Tempfiles create(Tempfiles tempfiles) {
        return tempfilesRepository.save(tempfiles);
    }

    public Tempfiles update(Tempfiles tempfiles) {
        return tempfilesRepository.save(tempfiles);
    }

    public void delete(Long tempfilesId) {
        tempfilesRepository.deleteById(tempfilesId);
    }
}
