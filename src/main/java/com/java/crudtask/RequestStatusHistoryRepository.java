package com.java.crudtask;

import com.java.crudtask.RequestStatusHistory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestStatusHistoryRepository extends CrudRepository<RequestStatusHistory,Long>{
}
