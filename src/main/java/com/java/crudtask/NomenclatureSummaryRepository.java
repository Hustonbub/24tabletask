package com.java.crudtask;

import com.java.crudtask.NomenclatureSummary;
import org.springframework.data.repository.CrudRepository;

public interface NomenclatureSummaryRepository extends CrudRepository<NomenclatureSummary,Long> {
}
