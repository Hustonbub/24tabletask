package com.java.crudtask;

import com.java.crudtask.FileRouting;
import com.java.crudtask.FileRoutingRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileRoutingService {
    public final FileRoutingRepository fileRoutingRepository;

    public FileRoutingService(FileRoutingRepository fileRoutingRepository) {
        this.fileRoutingRepository = fileRoutingRepository;
    }

    public List<FileRouting> getAll(){
        return (List<FileRouting>) fileRoutingRepository.findAll();
    }

    public FileRouting getById(Long fileRoutingId) {
        return fileRoutingRepository.findById(fileRoutingId).orElse(null);
    }

    public FileRouting create(FileRouting fileRouting) {
        return fileRoutingRepository.save(fileRouting);
    }

    public FileRouting update(FileRouting fileRouting) {
        return fileRoutingRepository.save(fileRouting);
    }

    public void delete(Long fileRoutingId) {
        fileRoutingRepository.deleteById(fileRoutingId);
    }
}
