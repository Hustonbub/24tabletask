package com.java.crudtask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Crudtask {
    public static void main(String[] args) {
        SpringApplication.run(Crudtask.class, args);
    }
}
