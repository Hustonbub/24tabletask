package com.java.crudtask;

import com.java.crudtask.CompanyUnit;
import com.java.crudtask.CompanyUnitRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyUnitService {
    public final CompanyUnitRepository companyUnitRepository;

    public CompanyUnitService(CompanyUnitRepository companyUnitRepository) {
        this.companyUnitRepository = companyUnitRepository;
    }

    public List<CompanyUnit> getAll() {
        return (List<CompanyUnit>) companyUnitRepository.findAll();
    }

    public CompanyUnit getById(Long companyUnitId) {
        return companyUnitRepository.findById(companyUnitId).orElse(null);
    }

    public CompanyUnit create(CompanyUnit companyUnit) {
        return companyUnitRepository.save(companyUnit);
    }

    public CompanyUnit update(CompanyUnit companyUnit) {
        return companyUnitRepository.save(companyUnit);
    }

    public void delete(Long companyUnitId) {
        companyUnitRepository.deleteById(companyUnitId);
    }
}
