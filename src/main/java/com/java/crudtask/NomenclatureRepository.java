package com.java.crudtask;

import com.java.crudtask.Nomenclature;
import org.springframework.data.repository.CrudRepository;

public interface NomenclatureRepository extends CrudRepository<Nomenclature,Long> {
}
