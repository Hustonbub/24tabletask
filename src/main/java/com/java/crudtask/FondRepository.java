package com.java.crudtask;

import com.java.crudtask.Fond;
import org.springframework.data.repository.CrudRepository;

public interface FondRepository extends CrudRepository<Fond,Long> {
}
