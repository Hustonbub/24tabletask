package com.java.crudtask;

import com.java.crudtask.File;
import org.springframework.data.repository.CrudRepository;

public interface FileRepository extends CrudRepository<File,Long> {
}
