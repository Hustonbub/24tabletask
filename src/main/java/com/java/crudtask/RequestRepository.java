package com.java.crudtask;

import com.java.crudtask.Request;
import org.springframework.data.repository.CrudRepository;

public interface RequestRepository extends CrudRepository<Request,Long> {
}
