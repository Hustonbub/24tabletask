package com.java.crudtask;

import com.java.crudtask.Users;
import com.java.crudtask.UsersRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersService {
    public final UsersRepository usersRepository;

    public UsersService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    public List<Users> getAll(){
        return (List<Users>) usersRepository.findAll();
    }

    public Users getById(Long usersId) {
        return usersRepository.findById(usersId).orElse(null);
    }

    public Users create(Users users) {
        return usersRepository.save(users);
    }

    public Users update(Users users) {
        return usersRepository.save(users);
    }

    public void delete(Long usersId) {
        usersRepository.deleteById(usersId);
    }


}
