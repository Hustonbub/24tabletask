package com.java.crudtask;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FileRoutingController {
    private final FileRoutingService fileRoutingService;

    public FileRoutingController(FileRoutingService fileRoutingService){
        this.fileRoutingService = fileRoutingService;
    }

    @GetMapping("fileRouting/{fileRoutingId}")
    public ResponseEntity<?> getFileRouting(@PathVariable Long fileRoutingId){
        return ResponseEntity.ok(fileRoutingService.getById(fileRoutingId));
    }

    @GetMapping("fileRouting")
    public ResponseEntity<?> getFileRouting(){
        return ResponseEntity.ok(fileRoutingService.getAll());
    }

    @PostMapping("fileRouting")
    public ResponseEntity<?> saveFileRouting(@RequestBody FileRouting fileRouting) {
        return ResponseEntity.ok(fileRoutingService.create(fileRouting));
    }

    @PutMapping("fileRouting")
    public ResponseEntity<?> updateFileRouting(@RequestBody FileRouting fileRouting) {
        return ResponseEntity.ok(fileRoutingService.update(fileRouting));
    }

    @DeleteMapping("fileRouting/{fileRoutingId}")
    public void deleteFileRouting(@PathVariable Long fileRoutingId) {
        fileRoutingService.delete(fileRoutingId);
    }
}
