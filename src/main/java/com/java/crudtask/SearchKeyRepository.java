package com.java.crudtask;

import com.java.crudtask.SearchKey;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchKeyRepository extends CrudRepository<SearchKey,Long> {
}
