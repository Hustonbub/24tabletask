package com.java.crudtask;

import com.java.crudtask.Notification;
import com.java.crudtask.NotificationRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotificationService {
    public final NotificationRepository notificationRepository;

    public NotificationService(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    public List<Notification> getAll(){
        return (List<Notification>) notificationRepository.findAll();
    }

    public Notification getById(Long notificationId) {
        return notificationRepository.findById(notificationId).orElse(null);
    }

    public Notification create(Notification notification) {
        return notificationRepository.save(notification);
    }

    public Notification update(Notification notification) {
        return notificationRepository.save(notification);
    }

    public void delete(Long notificationId) {
        notificationRepository.deleteById(notificationId);
    }
}
