package com.java.crudtask;

import com.java.crudtask.RequestStatusHistory;
import com.java.crudtask.RequestStatusHistoryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RequestStatusHistoryService {
    public final RequestStatusHistoryRepository requestStatusHistoryRepository;

    public RequestStatusHistoryService(RequestStatusHistoryRepository requestStatusHistoryRepository) {
        this.requestStatusHistoryRepository = requestStatusHistoryRepository;
    }

    public List<RequestStatusHistory> getAll(){
        return (List<RequestStatusHistory>) requestStatusHistoryRepository.findAll();
    }

    public RequestStatusHistory getById(Long requestStatusHistoryId) {
        return requestStatusHistoryRepository.findById(requestStatusHistoryId).orElse(null);
    }

    public RequestStatusHistory create(RequestStatusHistory requestStatusHistory) {
        return requestStatusHistoryRepository.save(requestStatusHistory);
    }

    public RequestStatusHistory update(RequestStatusHistory requestStatusHistory) {
        return requestStatusHistoryRepository.save(requestStatusHistory);
    }

    public void delete(Long requestStatusHistoryId) {
        requestStatusHistoryRepository.deleteById(requestStatusHistoryId);
    }
}
