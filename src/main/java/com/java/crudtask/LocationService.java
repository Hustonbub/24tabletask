package com.java.crudtask;

import com.java.crudtask.Location;
import com.java.crudtask.LocationRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationService {
    public final LocationRepository locationRepository;

    public LocationService(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    public List<Location> getAll(){
        return (List<Location>) locationRepository.findAll();
    }

    public Location getById(Long locationId) {
        return locationRepository.findById(locationId).orElse(null);
    }

    public Location create(Location location) {
        return locationRepository.save(location);
    }

    public Location update(Location location) {
        return locationRepository.save(location);
    }

    public void delete(Long locationId) {
        locationRepository.deleteById(locationId);
    }
}
