package com.java.crudtask;

import com.java.crudtask.Notification;
import org.springframework.data.repository.CrudRepository;

public interface NotificationRepository extends CrudRepository<Notification,Long> {
}