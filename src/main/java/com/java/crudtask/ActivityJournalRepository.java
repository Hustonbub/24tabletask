package com.java.crudtask;

import com.java.crudtask.ActivityJournal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityJournalRepository extends CrudRepository<ActivityJournal,Long> {
}
