package com.java.crudtask;

import com.java.crudtask.ActivityJournal;
import com.java.crudtask.ActivityJournalRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityJournalService {
    public final ActivityJournalRepository activityJournalRepository;

    public ActivityJournalService(ActivityJournalRepository activityJournalRepository) {
        this.activityJournalRepository = activityJournalRepository;
    }

    public List<ActivityJournal> getAll(){
        return (List<ActivityJournal>) activityJournalRepository.findAll();
    }

    public ActivityJournal getById(Long activityJournalId) {
        return activityJournalRepository.findById(activityJournalId).orElse(null);
    }

    public ActivityJournal create(ActivityJournal activityJournal) {
        return activityJournalRepository.save(activityJournal);
    }

    public ActivityJournal update(ActivityJournal activityJournal) {
        return activityJournalRepository.save(activityJournal);
    }

    public void delete(Long activityJournalId) {
        activityJournalRepository.deleteById(activityJournalId);
    }
}
