package com.java.crudtask;

import com.java.crudtask.SearchKeyRouting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchKeyRoutingRepository extends CrudRepository<SearchKeyRouting,Long> {
}
