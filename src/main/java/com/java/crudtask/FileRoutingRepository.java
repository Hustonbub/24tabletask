package com.java.crudtask;

import com.java.crudtask.FileRouting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface FileRoutingRepository extends CrudRepository<FileRouting,Long> {
}