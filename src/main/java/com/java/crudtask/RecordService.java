package com.java.crudtask;

import com.java.crudtask.Record;
import com.java.crudtask.RecordRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecordService {
    public final RecordRepository recordRepository;

    public RecordService(RecordRepository recordRepository) {
        this.recordRepository = recordRepository;
    }

    public List<Record> getAll(){
        return (List<Record>) recordRepository.findAll();
    }

    public Record getById(Long recordId) {
        return recordRepository.findById(recordId).orElse(null);
    }

    public Record create(Record record){
        return recordRepository.save(record);
    }

    public Record update(Record record) {
        return recordRepository.save(record);
    }

    public void delete(Long recordId) {
        recordRepository.deleteById(recordId);
    }
}
